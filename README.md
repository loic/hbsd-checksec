hbsd-checksec
========

hbsd-checksec is a script to check the properties of executables (like PIE, RELRO, CFI, Canaries, ASLR).

It has been originally written by Tobias Klein and the original source is available [here](http://www.trapkit.de/tools/checksec.html).

How to run this script
--------

```
$ fetch https://git.hardenedbsd.org/loic/hbsd-checksec/-/raw/master/hbsd-checksec
$ chmod 710 hbsd-checksec 
$ ./hbsd-checksec
```


Usage
--------

```
Options:

 ## hbsd-checksec Options
  --file={file}
  --dir={directory}
  --listfile={text file with one file per line}
  --proc={process name}
  --proc-all
  --proc-libs={process ID}
  --kernel
  --version
  --help

 ## Modifiers
  --debug
  --verbose
  --format={cli,csv,xml,json}
  --output={cli,csv,xml,json}
  --no-extended
```


Examples
--------

![alt text](Example_proc-all.png)
